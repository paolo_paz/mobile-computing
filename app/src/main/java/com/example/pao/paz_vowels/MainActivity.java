package com.example.pao.paz_vowels;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    public Button btn_go;
    public TextView tv_cons;
    public TextView tv_vowel;
    public TextView tv_word;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv_cons = (TextView) findViewById(R.id.tv_cons);
        tv_vowel = (TextView) findViewById(R.id.tv_vowel);
        tv_word = (EditText) findViewById(R.id.tv_word);
        btn_go = (Button) findViewById(R.id.btn_go);

        btn_go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ch = tv_word.getText().toString();

                String word = ch.replaceAll("[aeuoiAEIOU]", "");
                tv_cons.setText(word);

                int x = ch.length();
                int y = word.length();
                int ans = x - y;
                tv_vowel.setText(""+ans);
            }
        });
    }
}
